/*
Author: 
DEFRETIERE Clément

Sprites: 
rvros
https://rvros.itch.io/
*/

window.onload = function(){

var WIDTH, HEIGHT;
var FPS = 50;
var CLEAR_COL = "#444";

var GRAV = 0.8;

var EDGEERR = 20;

var holder = document.getElementById("gameHolder");
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

ctx.imageSmoothingEnabled = false;
ctx.mozImageSmoothingEnabled = false;

var keyboard = {
    up: false,
    down: false,
    right: false,
    left: false,
    space: false
}

var camera = {
    x: 0,
    y: 0,
    zoom: 1,
    zoomSpeed: 0,
    zoomMaxSpeed: 0.03,
    zoomAccel: 0,
    zoomIDLE: 3,
    zoomRun: 1.8,
    timerMove: 0,
    timerMoveEnd: 300
}

var blocks = [];
var newblockstart;

var players = [];
var me;

var XcoordWtoR = function(x){
    return ((x - camera.x) * camera.zoom) + 0.5 * WIDTH;
};

var YcoordWtoR = function(y){
    return ((y - camera.y) * camera.zoom) + 0.5 * HEIGHT;
};

var XcoordRtoW = function(x){
    return (x - 0.5 * WIDTH) / camera.zoom + camera.x;
};

var YcoordRtoW = function(y){
    return (y - 0.5 * HEIGHT) / camera.zoom + camera.y;
};

var loadAnim = function(path, nb, repeat=true, speed=5, ext=".png", len=2){
    var imgs = [];
    for(var i = 0; i < nb; i++){
        var img = new Image();
        var src = i.toString();
        while(src.length < len)
            src = "0" + src;

        img.src = path + src + ext;
        imgs.push(img);
    }
    return {
        frames: imgs,
        speed: speed,
        repeat: repeat
    };
}

var changeAnim = function(p, anim, offset=0){
    if(anim == p.anim)
        return;
    p.anim = anim;
    p.animIndex = offset;
}

var animIDLE = loadAnim("assets/adventurer-idle-", 3, true);
var animRun = loadAnim("assets/adventurer-run-", 6, true, 17);
var animJump = loadAnim("assets/adventurer-jump-", 4, false, 17);
var animGrb = loadAnim("assets/adventurer-crnr-grb-", 4);

var updateDims = function(){
    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;
    canvas.setAttribute("width", WIDTH);
    canvas.setAttribute("height", HEIGHT);
    ctx.imageSmoothingEnabled = false;
    ctx.mozImageSmoothingEnabled = false;
}

var newPlayer = function(){
    p = {
        x: 200,
        y: 150,
        color: "#fff",
        w: 40,
        h: 60,
        renderw: 100,
        renderh: 74,
        spriteOffX: 29,
        spriteOffY: 13,
        speedx: 0,
        speedy: 0,
        looksRight: true,
        maxspeed: 7,
        local: true,
        jumpForce: 13,
        doubleJumpAvailable: false,
        anim: animIDLE,
        animIndex: 0,
        onGround: false,
        isMoving: false,
        specialState: {
            grabbing: false
        }
    };
    if(p.local)
        me = p;
    players.push(p);
    return p;
}

var clearCanvas = function(){
    var lg = ctx.createLinearGradient(0, 0, WIDTH*2, HEIGHT*2);
    lg.addColorStop(1, "#ffa17f");
    lg.addColorStop(0, "#00223e");
    ctx.fillStyle = lg;
    ctx.fillRect(0, 0, WIDTH, HEIGHT);
}

function drawImage(img, x, y, width, height, deg, flip, flop, center) {
    ctx.save();

    if(typeof width === "undefined") width = img.width;
    if(typeof height === "undefined") height = img.height;
    if(typeof center === "undefined") center = false;

    // Set rotation point to center of image, instead of top/left
    if(center) {
        x -= width/2;
        y -= height/2;
    }

    // Set the origin to the center of the image
    ctx.translate(x + width/2, y + height/2);

    // Rotate the canvas around the origin
    var rad = 2 * Math.PI - deg * Math.PI / 180;    
    ctx.rotate(rad);

    // Flip/flop the canvas
    if(flip) flipScale = -1; else flipScale = 1;
    if(flop) flopScale = -1; else flopScale = 1;
    ctx.scale(flipScale, flopScale);

    // Draw the image    
    ctx.drawImage(img, -width/2, -height/2, width, height);

    ctx.restore();
}

var drawPlayer = function(p){
    //ctx.fillStyle = p.color;
    //ctx.fillRect(XcoordWtoR(p.x), YcoordWtoR(p.y), p.w * camera.zoom, p.h * camera.zoom);
    drawImage(p.anim.frames[Math.round(p.animIndex)], XcoordWtoR(p.x-p.spriteOffX), YcoordWtoR(p.y-p.spriteOffY), p.renderw*camera.zoom, p.renderh * camera.zoom, 0, !p.looksRight, false);
}

var drawPlayers = function(){
    for(var i = 0; i < players.length; i++){
        drawPlayer(players[i]);
    }
}

var updatePlayers = function(){
    for(var i = 0; i < players.length; i++){
        var p = players[i];
        
        // speed and movement
        if(p.local){
            p.speedx = players[i].maxspeed*(keyboard.right-keyboard.left);
        }
        if(!p.specialState.grabbing)
            p.speedy += GRAV;

        p.x += Math.round(players[i].speedx);
        p.y += Math.round(players[i].speedy);

        // grab edge
        if(p.local && p.specialState.grabbing){
            if(p.isMoving || p.onGround)
                p.specialState.grabbing = false;
        }
        if(p.local && keyboard.space){ 
            for(var j = 0; j < blocks.length; j++){
                b = blocks[j];
                if(equalerr(p.y, b.y, EDGEERR)){
                    if(equalerr(p.x, b.x + b.w, EDGEERR)){
                        p.specialState.grabbing = true;
                        p.speedy = 0;
                        p.y = b.y;
                        p.x = b.x + b.w;
                        p.doubleJumpAvailable = true;
                    }else if(equalerr(p.x + p.w, b.x, EDGEERR)){
                        p.specialState.grabbing = true;
                        p.speedy = 0;
                        p.y = b.y;
                        p.x = b.x;
                        p.doubleJumpAvailable = true;
                    }
                }
            }
        }

        // player dir
        if(Math.round(p.speedx) > 0){
            p.looksRight = true;
        }else if(Math.round(p.speedx) < 0){
            p.looksRight = false;
        }
        
        // anim loop
        p.animIndex += p.anim.speed/100;
        if(!p.anim.repeat && p.animIndex >= p.anim.frames.length - 1)
            p.animIndex = p.anim.frames.length -1;
        else
            p.animIndex %= p.anim.frames.length - 1;

        // move timer
        if(!p.isMoving)
            camera.timerMove++;
        if(camera.timerMove > camera.timerMoveEnd){
            camera.timerMove = camera.timerMoveEnd;
            camera.zoomAccel = 0.05;
        }else{
            camera.zoomAccel = -0.01;
        }

        // anim handling
        var nextAnim = p.anim;
        var nextOffset = 0;
        if(p.onGround){
            nextAnim = animIDLE;
        }
        if(p.onGround && p.isMoving){
            nextAnim = animRun;
        }
        if(!p.onGround){
            nextAnim = animJump;
            nextOffset = animJump.frames.length - 1;
        }
        if(p.specialState.grabbing)
            nextAnim = animGrb;
        // apply nextAnim
        if(p.anim.repeat || p.animIndex >= p.anim.frames.length - 1)
            changeAnim(p, nextAnim, nextOffset);
    }
}

var playerJump = function(p){
    if(p.onGround){
        p.speedy -= p.jumpForce;
        changeAnim(p, animJump);
    }
    else if(p.doubleJumpAvailable && p.speedy > -p.jumpForce){
        p.speedy = -p.jumpForce;
        p.doubleJumpAvailable = false;
        p.specialState.grabbing = false;
    }
}

var newBlock = function(x, y, w, h){
    b = {
        x: x,
        y: y,
        w: w,
        h: h,
        color: "#888"
    };
    
    blocks.push(b);
    return b;
}

var drawBlock = function(b){
    ctx.fillStyle = b.color;
    ctx.fillRect(XcoordWtoR(b.x), YcoordWtoR(b.y), b.w * camera.zoom, b.h * camera.zoom);
}

var drawBlocks = function(){
    for(var i = 0; i < blocks.length; i++)
        drawBlock(blocks[i]);
}

var isColliding = function(a, b){
    return (a.x < b.x + b.w && b.x < a.x + a.w && a.y < b.y + b.h && b.y < a.y + a.h);
}

var equalerr = function(a, b, p){
    return (b-p < a && a < b+p);
}

var applyCollisions = function(){
    for(var i = 0; i < players.length; i++){
        var p = players[i];
        var wasOnGround = p.onGround;
        var groundFound = false;
        for(var j = 0; j < blocks.length; j++){
            var b = blocks[j];
            if(!groundFound)
                p.onGround = false;
            if(isColliding(p, b)){
                var distance = new Array(8).fill(0);
                var minDist = 0;
                var xsav = p.x;
                var ysav = p.y;
                for(var k = 0; k < 8; k++){
                    while(isColliding(p, b)){
                        p.x += Math.round(Math.cos(k*0.79));
                        p.y += Math.round(Math.sin(k*0.79));
                        distance[k]++;
                    } 
                    if(distance[k] < distance[minDist] || (distance[k] == distance[minDist] && k%2 == 0))
                        minDist = k;
                    p.x = xsav;
                    p.y = ysav;
                }
                if(equalerr(Math.cos(minDist*0.79), 0, 0.1)){
                    p.speedy = 0;
                    if(Math.sin(minDist*0.79) < 0 && !groundFound){
                        p.onGround = true;
                        groundFound = true;
                        if(!wasOnGround){
                            var touchGroundEv = new CustomEvent("touchGround", {detail: p});
                            canvas.dispatchEvent(touchGroundEv);
                        }
                    }
                }
                if(equalerr(Math.sin(minDist*0.79), 0, 0.1))
                    p.speedx = 0;
                p.x += Math.round(Math.cos(minDist*0.79))*distance[minDist];
                p.y += Math.round(Math.sin(minDist*0.79))*distance[minDist]; 
            }
        }
        if(!groundFound && wasOnGround){
            var quitGroundEv = new CustomEvent("quitGround", {detail: p});
            canvas.dispatchEvent(quitGroundEv);
        }
    }
}

var updateCamera = function(){
    camera.x = me.x + 0.5 * me.w;
    camera.y = me.y + 0.5 * me.h;

    camera.zoomSpeed += camera.zoomAccel;
    if(camera.zoomSpeed > camera.zoomMaxSpeed)
        camera.zoomSpeed = camera.zoomMaxSpeed;
    else if(camera.zoomSpeed < -camera.zoomMaxSpeed)
        camera.zoomSpeed = -camera.zoomMaxSpeed;

    camera.zoom += camera.zoomSpeed;
    if(camera.zoom > camera.zoomIDLE)
        camera.zoom = camera.zoomIDLE;
    else if(camera.zoom < camera.zoomRun)
        camera.zoom = camera.zoomRun;
}

canvas.addEventListener("touchGround", function(e){
    var p = e.detail;
    p.doubleJumpAvailable = true;
    camera.zoomAccel = 0.05;
});

canvas.addEventListener("quitGround", function(e){
    var p = e.detail; 
    camera.zoomAccel = -0.01;
});

canvas.addEventListener("startMoving", function(e){
    var p = e.detail;
    camera.timerMove = 0;
});

canvas.addEventListener("stopMoving", function(e){
    var p = e.detail;
});

canvas.addEventListener("mousedown", function(e){
    newblockstart = {
        x: e.clientX,
        y: e.clientY
    };
});

canvas.addEventListener("mouseup", function(e){
    var x = Math.min(e.clientX, newblockstart.x);
    var y = Math.min(e.clientY, newblockstart.y);
    var w = Math.max(e.clientX, newblockstart.x) - Math.min(e.clientX, newblockstart.x);
    var h = Math.max(e.clientY, newblockstart.y) - Math.min(e.clientY, newblockstart.y);
    
    newBlock(XcoordRtoW(x), YcoordRtoW(y), w / camera.zoom, h / camera.zoom);
});

window.addEventListener("resize", updateDims);

var checkStartStopMoving = function(){
    if(keyboard.right - keyboard.left != 0){
        if(!me.isMoving){
            var startMovingEv = new CustomEvent("startMoving", {detail: me});
            canvas.dispatchEvent(startMovingEv);
            me.isMoving = true;
        }
    }else{
        if(me.isMoving){
            var stopMovingEv = new CustomEvent("stopMoving", {detail: me});
            canvas.dispatchEvent(stopMovingEv);
            me.isMoving = false;
        }
    }
}

document.addEventListener("keydown", function(e){
    if(e.key == "ArrowLeft")
        keyboard.left = true;
    else if(e.key == "ArrowRight")
        keyboard.right = true;
    else if(e.key == "ArrowUp"){
        keyboard.up = true;
        playerJump(me);
    }else if(e.key == "ArrowDown")
        keyboard.down = true;
    else if(e.code == "Space")
        keyboard.space = true;
    checkStartStopMoving();
});

document.addEventListener("keyup", function(e){
    if(e.key == "ArrowLeft")
        keyboard.left = false;
    else if(e.key == "ArrowRight")
        keyboard.right = false;
    else if(e.key == "ArrowUp")
        keyboard.up = false;
    else if(e.key == "ArrowDown")
        keyboard.down = false;
    else if(e.code == "Space")
        keyboard.space = false;
    checkStartStopMoving();
});

updateDims();

newBlock(50, 300, 500, 40);
newPlayer();

setInterval(function(){
    clearCanvas();
    updatePlayers();
    applyCollisions(); 
    updateCamera(); 
    drawPlayers();
    drawBlocks();
}, 1000/FPS);

} // ONLOAD
